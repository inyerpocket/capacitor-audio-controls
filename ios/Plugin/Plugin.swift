import Foundation
import Capacitor

/**
 * Please read the Capacitor iOS Plugin Development Guide
 * here: https://capacitorjs.com/docs/plugins/ios
 */
@objc(AudioControls)
public class AudioControls: CAPPlugin {

    @objc func initialise(_ call: CAPPluginCall) {
        call.success()
    }

    @objc func create(_ call: CAPPluginCall) {
        let value = call.getString("value") ?? ""
        call.success()
    }

    @objc func setTime(_ call: CAPPluginCall) {
        let duration = call.getInt("duration") ?? 0
        let position = call.getInt("position") ?? 0
        let isPlaying = call.getBool("duration") ?? false
        call.success()
    }

    @objc func destroy(_ call: CAPPluginCall) {
        call.success()
    }
}
