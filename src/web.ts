import { WebPlugin } from '@capacitor/core';
import { AudioControlsPlugin,CreateOptions,TimeOptions } from './definitions';

export class AudioControlsWeb extends WebPlugin implements AudioControlsPlugin {
  constructor() {
    super({
      name: 'AudioControls',
      platforms: ['web'],
    });
  }

  async initialise(): Promise<void> {
    console.log('initialise', 'not implemented');
    return;
  }

  async create(options: CreateOptions): Promise<void> {
    console.log('create', 'not implemented');
    return;
  }

  async updateTime(options: TimeOptions): Promise<void> {
    console.log('updateTime', 'not implemented');
    return;
  }

  async destroy(): Promise<void> {
    console.log('destroy', 'not implemented');
    return;
  }
}

const AudioControls = new AudioControlsWeb();

export { AudioControls };

import { registerWebPlugin } from '@capacitor/core';
registerWebPlugin(AudioControls);
