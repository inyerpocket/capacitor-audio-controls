declare module '@capacitor/core' {
  interface PluginRegistry {
    AudioControls: AudioControlsPlugin;
  }
}

export interface AudioControlsPlugin {
  initialise(): Promise<void>;
  create(options: CreateOptions): Promise<void>;
  updateTime(options: CreateOptions): Promise<void>;
  destroy(): Promise<void>;
}

export interface CreateOptions {
  title: string;
  artists: string;
  album: string;
  coverImage: string;
  hasPrev: boolean;
  hasNext: boolean;
  hasScrubbing: boolean;
  isPlaying: boolean;
  duration: number;
  position: number;
  isLive: boolean;
}

export interface TimeOptions {
  duration: number;
  position: number;
}
